#!/usr/bin/env bash
set -xeuo pipefail

prepare_build() {
    tar xf $1
    pushd ${1%.tar.*}
}

finish_build() {
    popd
    rm -rf ${1%.tar.*}
}

pushd /sources

if [ ! -f man-pages-5.13.tar.xz.built ]; then
    prepare_build man-pages-5.13.tar.xz
    make prefix=/usr install
    finish_build man-pages-5.13.tar.xz
    touch man-pages-5.13.tar.xz.built
fi

if [ ! -f iana-etc-20210611.tar.gz.built ]; then
    prepare_build iana-etc-20210611.tar.gz
    cp services protocols /etc
    finish_build iana-etc-20210611.tar.gz
    touch iana-etc-20210611.tar.gz.built
fi

if [ ! -f glibc-2.34.tar.xz.built2 ]; then
    prepare_build glibc-2.34.tar.xz
    sed -e '/NOTIFY_REMOVED)/s/)/ \&\& data.attr != NULL)/' \
    -i sysdeps/unix/sysv/linux/mq_notify.c
    patch -Np1 -i ../glibc-2.34-fhs-1.patch
    mkdir -v build
    cd       build
    echo "rootsbindir=/usr/bin" > configparms
    ../configure --prefix=/usr                            \
    --disable-werror                         \
    --enable-kernel=3.2                      \
    --enable-stack-protector=strong          \
    --with-headers=/usr/include              \
    libc_cv_slibdir=/usr/lib
    make
    make check || true
    echo "Was the above test ok?"
    read -r -p "[Y/n] " response
    case $response in
        [nN][oO]|[nN])
            exit 1
        ;;
    esac
    touch /etc/ld.so.conf
    sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile
    make install
    sed '/RTLDLIST=/s@/usr@@g' -i /usr/bin/ldd
    cp -v ../nscd/nscd.conf /etc/nscd.conf
    mkdir -pv /var/cache/nscd
    mkdir -pv /usr/lib/locale
    localedef -i POSIX -f UTF-8 C.UTF-8 2> /dev/null || true
    localedef -i cs_CZ -f UTF-8 cs_CZ.UTF-8
    localedef -i de_DE -f ISO-8859-1 de_DE
    localedef -i de_DE@euro -f ISO-8859-15 de_DE@euro
    localedef -i de_DE -f UTF-8 de_DE.UTF-8
    localedef -i el_GR -f ISO-8859-7 el_GR
    localedef -i en_GB -f ISO-8859-1 en_GB
    localedef -i en_GB -f UTF-8 en_GB.UTF-8
    localedef -i en_HK -f ISO-8859-1 en_HK
    localedef -i en_PH -f ISO-8859-1 en_PH
    localedef -i en_US -f ISO-8859-1 en_US
    localedef -i en_US -f UTF-8 en_US.UTF-8
    localedef -i es_ES -f ISO-8859-15 es_ES@euro
    localedef -i es_MX -f ISO-8859-1 es_MX
    localedef -i fa_IR -f UTF-8 fa_IR
    localedef -i fr_FR -f ISO-8859-1 fr_FR
    localedef -i fr_FR@euro -f ISO-8859-15 fr_FR@euro
    localedef -i fr_FR -f UTF-8 fr_FR.UTF-8
    localedef -i is_IS -f ISO-8859-1 is_IS
    localedef -i is_IS -f UTF-8 is_IS.UTF-8
    localedef -i it_IT -f ISO-8859-1 it_IT
    localedef -i it_IT -f ISO-8859-15 it_IT@euro
    localedef -i it_IT -f UTF-8 it_IT.UTF-8
    localedef -i ja_JP -f EUC-JP ja_JP
    localedef -i ja_JP -f SHIFT_JIS ja_JP.SIJS 2> /dev/null || true
    localedef -i ja_JP -f UTF-8 ja_JP.UTF-8
    localedef -i nl_NL@euro -f ISO-8859-15 nl_NL@euro
    localedef -i ru_RU -f KOI8-R ru_RU.KOI8-R
    localedef -i ru_RU -f UTF-8 ru_RU.UTF-8
    localedef -i se_NO -f UTF-8 se_NO.UTF-8
    localedef -i ta_IN -f UTF-8 ta_IN.UTF-8
    localedef -i tr_TR -f UTF-8 tr_TR.UTF-8
    localedef -i zh_CN -f GB18030 zh_CN.GB18030
    localedef -i zh_HK -f BIG5-HKSCS zh_HK.BIG5-HKSCS
    localedef -i zh_TW -f UTF-8 zh_TW.UTF-8
    make localedata/install-locales
    localedef -i POSIX -f UTF-8 C.UTF-8 2> /dev/null || true
    localedef -i ja_JP -f SHIFT_JIS ja_JP.SIJS 2> /dev/null || true
    cat > /etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf

passwd: files
group: files
shadow: files

hosts: files dns
networks: files

protocols: files
services: files
ethers: files
rpc: files

# End /etc/nsswitch.conf
EOF
    tar -xf ../../tzdata2021a.tar.gz
    
    ZONEINFO=/usr/share/zoneinfo
    mkdir -pv $ZONEINFO/{posix,right}
    
    for tz in etcetera southamerica northamerica europe africa antarctica  \
    asia australasia backward; do
        zic -L /dev/null   -d $ZONEINFO       ${tz}
        zic -L /dev/null   -d $ZONEINFO/posix ${tz}
        zic -L leapseconds -d $ZONEINFO/right ${tz}
    done
    
    cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
    zic -d $ZONEINFO -p America/Chicago
    unset ZONEINFO
    ln -sfv /usr/share/zoneinfo/America/Chicago /etc/localtime
    cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib
/opt/lib

EOF
    cat >> /etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf

EOF
    mkdir -pv /etc/ld.so.conf.d
    finish_build glibc-2.34.tar.xz
    touch glibc-2.34.tar.xz.built2
fi

if [ ! -f zlib-1.2.11.tar.xz.built ]; then
    prepare_build zlib-1.2.11.tar.xz
    ./configure --prefix=/usr
    make
    make install
    rm -fv /usr/lib/libz.a
    finish_build zlib-1.2.11.tar.xz
    touch zlib-1.2.11.tar.xz.built
fi

if [ ! -f bzip2-1.0.8.tar.gz.built ]; then
    prepare_build bzip2-1.0.8.tar.gz
    patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch
    sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
    sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
    make -f Makefile-libbz2_so
    make clean
    make
    make PREFIX=/usr install
    cp -av libbz2.so.* /usr/lib
    ln -sv libbz2.so.1.0.8 /usr/lib/libbz2.so
    cp -v bzip2-shared /usr/bin/bzip2
    for i in /usr/bin/{bzcat,bunzip2}; do
        ln -sfv bzip2 $i
    done
    rm -fv /usr/lib/libbz2.a
    finish_build bzip2-1.0.8.tar.gz
    touch bzip2-1.0.8.tar.gz.built
fi

if [ ! -f xz-5.2.5.tar.xz.built2 ]; then
    prepare_build xz-5.2.5.tar.xz
    ./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/xz-5.2.5
    make
    make install
    finish_build xz-5.2.5.tar.xz
    touch xz-5.2.5.tar.xz.built2
fi

if [ ! -f zstd-1.5.0.tar.gz.built ]; then
    prepare_build zstd-1.5.0.tar.gz
    make
    make prefix=/usr install
    rm -v /usr/lib/libzstd.a
    finish_build zstd-1.5.0.tar.gz
    touch zstd-1.5.0.tar.gz.built
fi

if [ ! -f file-5.40.tar.gz.built2 ]; then
    prepare_build file-5.40.tar.gz
    ./configure --prefix=/usr
    make
    make install
    finish_build file-5.40.tar.gz
    touch file-5.40.tar.gz.built2
fi

if [ ! -f readline-8.1.tar.gz.built ]; then
    prepare_build readline-8.1.tar.gz
    sed -i '/MV.*old/d' Makefile.in
    sed -i '/{OLDSUFF}/c:' support/shlib-install
    ./configure --prefix=/usr    \
    --disable-static \
    --with-curses \
    --docdir=/usr/share/doc/readline-8.1
    make SHLIB_LIBS="-lncursesw"
    make SHLIB_LIBS="-lncursesw" install
    install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/readline-8.1
    finish_build readline-8.1.tar.gz
    touch readline-8.1.tar.gz.built
fi

if [ ! -f m4-1.4.19.tar.xz.built2 ]; then
    prepare_build m4-1.4.19.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build m4-1.4.19.tar.xz
    touch m4-1.4.19.tar.xz.built2
fi

if [ ! -f bc-5.0.0.tar.xz.built ]; then
    prepare_build bc-5.0.0.tar.xz
    CC=gcc ./configure --prefix=/usr -G -O3
    make
    make install
    finish_build bc-5.0.0.tar.xz
    touch bc-5.0.0.tar.xz.built
fi

if [ ! -f flex-2.6.4.tar.gz.built ]; then
    prepare_build flex-2.6.4.tar.gz
    ./configure --prefix=/usr \
    --docdir=/usr/share/doc/flex-2.6.4 \
    --disable-static
    make
    make install
    ln -sv flex /usr/bin/lex
    finish_build flex-2.6.4.tar.gz
    touch flex-2.6.4.tar.gz.built
fi

if [ ! -f tcl8.6.11-src.tar.gz.built ]; then
    tar xf tcl8.6.11-src.tar.gz
    pushd tcl8.6.11
    tar -xf ../tcl8.6.11-html.tar.gz --strip-components=1
    SRCDIR=$(pwd)
    cd unix
    ./configure --prefix=/usr           \
    --mandir=/usr/share/man \
    $([ "$(uname -m)" = x86_64 ] && echo --enable-64bit)
    make
    
    sed -e "s|$SRCDIR/unix|/usr/lib|" \
    -e "s|$SRCDIR|/usr/include|"  \
    -i tclConfig.sh
    
    sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.2|/usr/lib/tdbc1.1.2|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2/library|/usr/lib/tcl8.6|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2|/usr/include|"            \
    -i pkgs/tdbc1.1.2/tdbcConfig.sh
    
    sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.1|/usr/lib/itcl4.2.1|" \
    -e "s|$SRCDIR/pkgs/itcl4.2.1/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/itcl4.2.1|/usr/include|"            \
    -i pkgs/itcl4.2.1/itclConfig.sh
    
    unset SRCDIR
    make install
    chmod -v u+w /usr/lib/libtcl8.6.so
    make install-private-headers
    ln -sfv tclsh8.6 /usr/bin/tclsh
    mv /usr/share/man/man3/{Thread,Tcl_Thread}.3
    finish_build tcl8.6.11-src.tar.gz
    touch tcl8.6.11-src.tar.gz.built
fi

if [ ! -f expect5.45.4.tar.gz.built ]; then
    prepare_build expect5.45.4.tar.gz
    ./configure --prefix=/usr           \
    --with-tcl=/usr/lib     \
    --enable-shared         \
    --mandir=/usr/share/man \
    --with-tclinclude=/usr/include
    make
    make install
    ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib
    finish_build expect5.45.4.tar.gz
    touch expect5.45.4.tar.gz.built
fi

if [ ! -f dejagnu-1.6.3.tar.gz.built ]; then
    prepare_build dejagnu-1.6.3.tar.gz
    mkdir -v build
    cd       build
    ../configure --prefix=/usr
    makeinfo --html --no-split -o doc/dejagnu.html ../doc/dejagnu.texi
    makeinfo --plaintext       -o doc/dejagnu.txt  ../doc/dejagnu.texi
    make install
    install -v -dm755  /usr/share/doc/dejagnu-1.6.3
    install -v -m644   doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-1.6.3
    finish_build dejagnu-1.6.3.tar.gz
    touch dejagnu-1.6.3.tar.gz.built
fi

if [ ! -f binutils-2.37.tar.xz.built2 ]; then
    expect -c "spawn ls"
    prepare_build binutils-2.37.tar.xz
    patch -Np1 -i ../binutils-2.37-upstream_fix-1.patch
    sed -i '63d' etc/texi2pod.pl
    find -name \*.1 -delete
    mkdir -v build
    cd       build
    ../configure --prefix=/usr       \
    --enable-gold       \
    --enable-ld=default \
    --enable-plugins    \
    --enable-shared     \
    --disable-werror    \
    --enable-64-bit-bfd \
    --with-system-zlib
    make tooldir=/usr
    make -k check || true
    echo "Was the above test ok?"
    read -r -p "[Y/n] " response
    case "$response" in
        [nN][oO]|[nN])
            exit 1
        ;;
    esac
    make tooldir=/usr install -j1
    rm -fv /usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes}.a
    finish_build binutils-2.37.tar.xz
    touch binutils-2.37.tar.xz.built2
fi

if [ ! -f gmp-6.2.1.tar.xz.built ]; then
    prepare_build gmp-6.2.1.tar.xz
    ./configure --prefix=/usr    \
    --enable-cxx         \
    --disable-static     \
    --docdir=/usr/share/doc/gmp-6.2.1
    make
    make html
    make check 2>&1 | tee gmp-check-log
    awk '/# PASS:/{total+=$3} ; END{print total}' gmp-check-log
    make install
    make install-html
    finish_build gmp-6.2.1.tar.xz
    touch gmp-6.2.1.tar.xz.built
fi

if [ ! -f mpfr-4.1.0.tar.xz.built ]; then
    prepare_build mpfr-4.1.0.tar.xz
    ./configure --prefix=/usr    \
    --disable-static       \
    --enable-thread-safe  \
    --docdir=/usr/share/doc/mpfr-4.1.0
    make
    make html
    make check
    make install
    make install-html
    finish_build mpfr-4.1.0.tar.xz
    touch mpfr-4.1.0.tar.xz.built
fi

if [ ! -f mpc-1.2.1.tar.gz.built ]; then
    prepare_build mpc-1.2.1.tar.gz
    ./configure --prefix=/usr    \
    --disable-static       \
    --docdir=/usr/share/doc/mpc-1.2.1
    make
    make html
    make install
    make install-html
    finish_build mpc-1.2.1.tar.gz
    touch mpc-1.2.1.tar.gz.built
fi

if [ ! -f attr-2.5.1.tar.gz.built ]; then
    prepare_build attr-2.5.1.tar.gz
    ./configure --prefix=/usr     \
    --disable-static  \
    --sysconfdir=/etc \
    --docdir=/usr/share/doc/attr-2.5.1
    make
    make install
    finish_build attr-2.5.1.tar.gz
    touch attr-2.5.1.tar.gz.built
fi

if [ ! -f acl-2.3.1.tar.xz.built ]; then
    prepare_build acl-2.3.1.tar.xz
    ./configure --prefix=/usr         \
    --disable-static      \
    --docdir=/usr/share/doc/acl-2.3.1
    make
    make install
    finish_build acl-2.3.1.tar.xz
    touch acl-2.3.1.tar.xz.built
fi

if [ ! -f libcap-2.53.tar.xz.built ]; then
    prepare_build libcap-2.53.tar.xz
    sed -i '/install -m.*STA/d' libcap/Makefile
    make prefix=/usr lib=lib
    make prefix=/usr lib=lib install
    chmod -v 755 /usr/lib/lib{cap,psx}.so.2.53
    finish_build libcap-2.53.tar.xz
    touch libcap-2.53.tar.xz.built
fi

if [ ! -f shadow-4.9.tar.xz.built ]; then
    prepare_build shadow-4.9.tar.xz
    sed -i 's/groups$(EXEEXT) //' src/Makefile.in
    find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
    find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
    find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;
    sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
    -e 's:/var/spool/mail:/var/mail:'                 \
    -e '/PATH=/{s@/sbin:@@;s@/bin:@@}'                \
    -i etc/login.defs
    sed -e "224s/rounds/min_rounds/" -i libmisc/salt.c
    touch /usr/bin/passwd
    ./configure --sysconfdir=/etc \
    --with-group-name-max-length=32
    make
    make exec_prefix=/usr install
    make -C man install-man
    mkdir -p /etc/default
    useradd -D --gid 999
    pwconv
    grpconv
    sed -i 's/yes/no/' /etc/default/useradd
    finish_build shadow-4.9.tar.xz
    touch shadow-4.9.tar.xz.built
fi

echo "Enter a root password."
passwd root

if [ ! -f gcc-11.2.0.tar.xz.built2 ]; then
    prepare_build gcc-11.2.0.tar.xz
    sed -e '/static.*SIGSTKSZ/d' \
    -e 's/return kAltStackSize/return SIGSTKSZ * 4/' \
    -i libsanitizer/sanitizer_common/sanitizer_posix_libcdep.cpp
    case $(uname -m) in
        x86_64)
            sed -e '/m64=/s/lib64/lib/' \
            -i.orig gcc/config/i386/t-linux64
        ;;
    esac
    mkdir -v build
    cd       build
    ../configure --prefix=/usr            \
    LD=ld                    \
    --enable-languages=c,c++ \
    --disable-multilib       \
    --disable-bootstrap      \
    --with-system-zlib
    make
    ulimit -s 32768
    chown -Rv tester .
    su tester -c "PATH=$PATH make -k check" || true
    ../contrib/test_summary
    make install
    rm -rf /usr/lib/gcc/$(gcc -dumpmachine)/11.2.0/include-fixed/bits/
    chown -v -R root:root \
    /usr/lib/gcc/*linux-gnu/11.2.0/include{,-fixed}
    ln -svr /usr/bin/cpp /usr/lib
    ln -sfv ../../libexec/gcc/$(gcc -dumpmachine)/11.2.0/liblto_plugin.so \
    /usr/lib/bfd-plugins/
    echo 'int main(){}' > dummy.c
    cc dummy.c -v -Wl,--verbose &> dummy.log
    readelf -l a.out | grep ': /lib'
    echo "Can you see the correct program interpreter (/lib64/ld-linux-x86-64.so.2)?"
    read -r -p "[Y/n] " response
    if [[ $response =~ ^[Nn]$ ]]; then
        exit 1
    fi
    grep -o '/usr/lib.*/crt[1in].*succeeded' dummy.log
    grep -B4 '^ /usr/include' dummy.log
    grep 'SEARCH.*/usr/lib' dummy.log |sed 's|; |\n|g'
    grep "/lib.*/libc.so.6 " dummy.log
    grep found dummy.log
    echo "All ok?"
    read -r -p "[Y/n] " response
    if [[ $response =~ ^[Nn]$ ]]; then
        exit 1
    fi
    rm -v dummy.c a.out dummy.log
    mkdir -pv /usr/share/gdb/auto-load/usr/lib
    mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib
    finish_build gcc-11.2.0.tar.xz
    touch gcc-11.2.0.tar.xz.built2
fi

if [ ! -f pkg-config-0.29.2.tar.gz.built ]; then
    prepare_build pkg-config-0.29.2.tar.gz
    ./configure --prefix=/usr              \
    --with-internal-glib       \
    --disable-host-tool        \
    --docdir=/usr/share/doc/pkg-config-0.29.2
    make
    make install
    finish_build pkg-config-0.29.2.tar.gz
    touch pkg-config-0.29.2.tar.gz.built
fi

if [ ! -f ncurses-6.2.tar.gz.built2 ]; then
    prepare_build ncurses-6.2.tar.gz
    ./configure --prefix=/usr           \
    --mandir=/usr/share/man \
    --with-shared           \
    --without-debug         \
    --without-normal        \
    --enable-pc-files       \
    --enable-widec
    make
    make install
    for lib in ncurses form panel menu ; do
        rm -vf                    /usr/lib/lib${lib}.so
        echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
        ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
    done
    rm -vf                     /usr/lib/libcursesw.so
    echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
    ln -sfv libncurses.so      /usr/lib/libcurses.so
    rm -fv /usr/lib/libncurses++w.a
    mkdir -v       /usr/share/doc/ncurses-6.2
    cp -v -R doc/* /usr/share/doc/ncurses-6.2
    finish_build ncurses-6.2.tar.gz
    touch ncurses-6.2.tar.gz.built2
fi

if [ ! -f sed-4.8.tar.xz.built2 ]; then
    prepare_build sed-4.8.tar.xz
    ./configure --prefix=/usr
    make
    make html
    make install
    install -d -m755           /usr/share/doc/sed-4.8
    install -m644 doc/sed.html /usr/share/doc/sed-4.8
    finish_build sed-4.8.tar.xz
    touch sed-4.8.tar.xz.built2
fi

if [ ! -f psmisc-23.4.tar.xz.built ]; then
    prepare_build psmisc-23.4.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build psmisc-23.4.tar.xz
    touch psmisc-23.4.tar.xz.built
fi

if [ ! -f gettext-0.21.tar.xz.built2 ]; then
    prepare_build gettext-0.21.tar.xz
    ./configure --prefix=/usr    \
    --disable-static \
    --docdir=/usr/share/doc/gettext-0.21
    make
    make install
    chmod -v 0755 /usr/lib/preloadable_libintl.so
    finish_build gettext-0.21.tar.xz
    touch gettext-0.21.tar.xz.built2
fi

if [ ! -f bison-3.7.6.tar.xz.built2 ]; then
    prepare_build bison-3.7.6.tar.xz
    ./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.7.6
    make
    make install
    finish_build bison-3.7.6.tar.xz
    touch bison-3.7.6.tar.xz.built2
fi

if [ ! -f grep-3.7.tar.xz.built2 ]; then
    prepare_build grep-3.7.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build grep-3.7.tar.xz
    touch grep-3.7.tar.xz.built2
fi

if [ ! -f bash-5.1.8.tar.gz.built2 ]; then
    prepare_build bash-5.1.8.tar.gz
    ./configure --prefix=/usr                      \
    --docdir=/usr/share/doc/bash-5.1.8 \
    --without-bash-malloc              \
    --with-installed-readline
    make
    make install
fi

popd

echo "Execute the new bash by running the below command:"
echo "exec /bin/bash --login +h"
echo "Then run lfs8.sh."