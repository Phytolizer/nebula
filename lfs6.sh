#!/usr/bin/env bash
set -xeuo pipefail

touch /var/log/{btmp,lastlog,faillog,wtmp}
chgrp -v utmp /var/log/lastlog
chmod -v 664  /var/log/lastlog
chmod -v 600  /var/log/btmp

prepare_build() {
    tar xf $1
    pushd ${1%.tar.*}
}

finish_build() {
    popd
    rm -rf ${1%.tar.*}
}

pushd /sources

if [ ! -f gcc-11.2.0.tar.xz.built-libstdcpp-pass2 ]; then
    prepare_build gcc-11.2.0.tar.xz
    ln -s gthr-posix.h libgcc/gthr-default.h
    mkdir -v build
    cd       build
    ../libstdc++-v3/configure            \
    CXXFLAGS="-g -O2 -D_GNU_SOURCE"  \
    --prefix=/usr                    \
    --disable-multilib               \
    --disable-nls                    \
    --host=$(uname -m)-lfs-linux-gnu \
    --disable-libstdcxx-pch
    make
    make install
    finish_build gcc-11.2.0.tar.xz
    touch gcc-11.2.0.tar.xz.built-libstdcpp-pass2
fi

if [ ! -f gettext-0.21.tar.xz.built ]; then
    prepare_build gettext-0.21.tar.xz
    ./configure --disable-shared
    make
    cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /usr/bin
    finish_build gettext-0.21.tar.xz
    touch gettext-0.21.tar.xz.built
fi

if [ ! -f bison-3.7.6.tar.xz.built ]; then
    prepare_build bison-3.7.6.tar.xz
    ./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.7.6
    make
    make install
    finish_build bison-3.7.6.tar.xz
    touch bison-3.7.6.tar.xz.built
fi

if [ ! -f perl-5.34.0.tar.xz.built ]; then
    prepare_build perl-5.34.0.tar.xz
    sh Configure -des                                        \
    -Dprefix=/usr                               \
    -Dvendorprefix=/usr                         \
    -Dprivlib=/usr/lib/perl5/5.34/core_perl     \
    -Darchlib=/usr/lib/perl5/5.34/core_perl     \
    -Dsitelib=/usr/lib/perl5/5.34/site_perl     \
    -Dsitearch=/usr/lib/perl5/5.34/site_perl    \
    -Dvendorlib=/usr/lib/perl5/5.34/vendor_perl \
    -Dvendorarch=/usr/lib/perl5/5.34/vendor_perl
    make
    make install
    finish_build perl-5.34.0.tar.xz
    touch perl-5.34.0.tar.xz.built
fi

if [ ! -f Python-3.9.6.tar.xz.built ]; then
    prepare_build Python-3.9.6.tar.xz
    ./configure --prefix=/usr   \
    --enable-shared \
    --without-ensurepip
    make
    make install
    finish_build Python-3.9.6.tar.xz
    touch Python-3.9.6.tar.xz.built
fi

if [ ! -f texinfo-6.8.tar.xz.built ]; then
    prepare_build texinfo-6.8.tar.xz
    sed -e 's/__attribute_nonnull__/__nonnull/' \
    -i gnulib/lib/malloc/dynarray-skeleton.c
    ./configure --prefix=/usr
    make
    make install
    finish_build texinfo-6.8.tar.xz
    touch texinfo-6.8.tar.xz.built
fi

if [ ! -f util-linux-2.37.2.tar.xz.built ]; then
    prepare_build util-linux-2.37.2.tar.xz
    mkdir -pv /var/lib/hwclock
    ./configure ADJTIME_PATH=/var/lib/hwclock/adjtime    \
    --libdir=/usr/lib    \
    --docdir=/usr/share/doc/util-linux-2.37.2 \
    --disable-chfn-chsh  \
    --disable-login      \
    --disable-nologin    \
    --disable-su         \
    --disable-setpriv    \
    --disable-runuser    \
    --disable-pylibmount \
    --disable-static     \
    --without-python     \
    runstatedir=/run
    make
    make install
    finish_build util-linux-2.37.2.tar.xz
    touch util-linux-2.37.2.tar.xz.built
fi

popd

echo "==== CLEANING UP ===="
rm -rf /usr/share/{info,man,doc}/*

find /usr/{lib,libexec} -name \*.la -delete
rm -rf /tools

echo "Now run lfs7.sh."