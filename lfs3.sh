#!/usr/bin/env bash
set -xeuo pipefail

echo $LFS

prepare_build() {
    tar xf $1
    pushd ${1%.tar.*}
}

finish_build() {
    popd
    rm -rf ${1%.tar.*}
}

pushd $LFS/sources

if [ ! -f binutils-2.37.tar.xz.built ]; then
    prepare_build binutils-2.37.tar.xz
    mkdir -v build
    cd       build
    ../configure --prefix=$LFS/tools \
    --with-sysroot=$LFS \
    --target=$LFS_TGT   \
    --disable-nls       \
    --disable-werror
    make
    make install -j1
    finish_build binutils-2.37.tar.xz
    touch binutils-2.37.tar.xz.built
fi

if [ ! -f gcc-11.2.0.tar.xz.built ]; then
    prepare_build gcc-11.2.0.tar.xz
    tar -xf ../mpfr-4.1.0.tar.xz
    mv -v mpfr-4.1.0 mpfr
    tar -xf ../gmp-6.2.1.tar.xz
    mv -v gmp-6.2.1 gmp
    tar -xf ../mpc-1.2.1.tar.gz
    mv -v mpc-1.2.1 mpc
    case $(uname -m) in
        x86_64)
            sed -e '/m64=/s/lib64/lib/' \
            -i.orig gcc/config/i386/t-linux64
        ;;
    esac
    mkdir -v build
    cd       build
    ../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=$LFS/tools                            \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++
    make
    make install
    cd ..
    cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
    `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/install-tools/include/limits.h
    finish_build gcc-11.2.0.tar.xz
    touch gcc-11.2.0.tar.xz.built
fi

if [ ! -f linux-5.13.12.tar.xz.built-headers ]; then
    prepare_build linux-5.13.12.tar.xz
    make mrproper
    make headers
    find usr/include -name '.*' -delete
    rm usr/include/Makefile
    cp -rv usr/include $LFS/usr
    finish_build linux-5.13.12.tar.xz
    touch linux-5.13.12.tar.xz.built-headers
fi

if [ ! -f glibc-2.34.tar.xz.built ]; then
    prepare_build glibc-2.34.tar.xz
    case $(uname -m) in
        i?86)   ln -sfv ld-linux.so.2 $LFS/lib/ld-lsb.so.3
        ;;
        x86_64) ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64
            ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64/ld-lsb-x86-64.so.3
        ;;
    esac
    patch -Np1 -i ../glibc-2.34-fhs-1.patch
    mkdir -v build
    cd       build
    echo "rootsbindir=/usr/sbin" > configparms
    ../configure                             \
    --prefix=/usr                      \
    --host=$LFS_TGT                    \
    --build=$(../scripts/config.guess) \
    --enable-kernel=3.2                \
    --with-headers=$LFS/usr/include    \
    libc_cv_slibdir=/usr/lib
    make
    make DESTDIR=$LFS install
    sed '/RTLDLIST=/s@/usr@@g' -i $LFS/usr/bin/ldd
    
    # Check that it works
    echo 'int main(){}' > dummy.c
    $LFS_TGT-gcc dummy.c
    readelf -l a.out | grep '/ld-linux'
    echo "Do you see the correct program interpreter (/lib64/ld-linux-x86-64.so.2)?"
    read -r -p "[Y/n] " response
    case $response in
        [nN][oO]|[nN])
            popd
            rm -rf glibc-2.34
            exit 1
        ;;
        *)
            echo "OK, continuing."
        ;;
    esac
    rm -v dummy.c a.out
    
    # Finalize installation of limits.h
    $LFS/tools/libexec/gcc/$LFS_TGT/11.2.0/install-tools/mkheaders
    
    finish_build glibc-2.34.tar.xz
    touch glibc-2.34.tar.xz.built
fi

if [ ! -f gcc-11.2.0.tar.xz.built-libstdcpp ]; then
    prepare_build gcc-11.2.0.tar.xz
    mkdir -v build
    cd       build
    ../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --build=$(../config.guess)      \
    --prefix=/usr                   \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/11.2.0
    make
    make DESTDIR=$LFS install
    finish_build gcc-11.2.0.tar.xz
    touch gcc-11.2.0.tar.xz.built-libstdcpp
fi

if [ ! -f m4-1.4.19.tar.xz.built ]; then
    prepare_build m4-1.4.19.tar.xz
    ./configure --prefix=/usr \
    --host=$LFS_TGT \
    --build=$(build-aux/config.guess)
    make
    make DESTDIR=$LFS install
    finish_build m4-1.4.19.tar.xz
    touch m4-1.4.19.tar.xz.built
fi

if [ ! -f ncurses-6.2.tar.gz.built ]; then
    prepare_build ncurses-6.2.tar.gz
    sed -i s/mawk// configure
    mkdir build
    pushd build
    ../configure
    make -C include
    make -C progs tic
    popd
    ./configure --prefix=/usr                \
    --host=$LFS_TGT              \
    --build=$(./config.guess)    \
    --mandir=/usr/share/man      \
    --with-manpage-format=normal \
    --with-shared                \
    --without-debug              \
    --without-ada                \
    --without-normal             \
    --enable-widec
    make
    make DESTDIR=$LFS TIC_PATH=$(pwd)/build/progs/tic install
    echo "INPUT(-lncursesw)" > $LFS/usr/lib/libncurses.so
    finish_build ncurses-6.2.tar.gz
    touch ncurses-6.2.tar.gz.built
fi

if [ ! -f bash-5.1.8.tar.gz.built ]; then
    prepare_build bash-5.1.8.tar.gz
    ./configure --prefix=/usr                   \
    --build=$(support/config.guess) \
    --host=$LFS_TGT                 \
    --without-bash-malloc
    make
    make DESTDIR=$LFS install
    ln -sv bash $LFS/bin/sh
    finish_build bash-5.1.8.tar.gz
    touch bash-5.1.8.tar.gz.built
fi

if [ ! -f coreutils-8.32.tar.xz.built ]; then
    prepare_build coreutils-8.32.tar.xz
    ./configure --prefix=/usr                     \
    --host=$LFS_TGT                   \
    --build=$(build-aux/config.guess) \
    --enable-install-program=hostname \
    --enable-no-install-program=kill,uptime
    make
    make DESTDIR=$LFS install
    mv -v $LFS/usr/bin/chroot                                     $LFS/usr/sbin
    mkdir -pv $LFS/usr/share/man/man8
    mv -v $LFS/usr/share/man/man1/chroot.1                        $LFS/usr/share/man/man8/chroot.8
    sed -i 's/"1"/"8"/'                                           $LFS/usr/share/man/man8/chroot.8
    finish_build coreutils-8.32.tar.xz
    touch coreutils-8.32.tar.xz.built
fi

if [ ! -f diffutils-3.8.tar.xz.built ]; then
    prepare_build diffutils-3.8.tar.xz
    ./configure --prefix=/usr --host=$LFS_TGT
    make
    make DESTDIR=$LFS install
    finish_build diffutils-3.8.tar.xz
    touch diffutils-3.8.tar.xz.built
fi

if [ ! -f file-5.40.tar.gz.built ]; then
    prepare_build file-5.40.tar.gz
    mkdir build
    pushd build
    ../configure --disable-bzlib      \
    --disable-libseccomp \
    --disable-xzlib      \
    --disable-zlib
    make
    popd
    ./configure --prefix=/usr --host=$LFS_TGT --build=$(./config.guess)
    make FILE_COMPILE=$(pwd)/build/src/file
    make DESTDIR=$LFS install
    finish_build file-5.40.tar.gz
    touch file-5.40.tar.gz.built
fi

if [ ! -f findutils-4.8.0.tar.xz.built ]; then
    prepare_build findutils-4.8.0.tar.xz
    ./configure --prefix=/usr                   \
    --localstatedir=/var/lib/locate \
    --host=$LFS_TGT                 \
    --build=$(build-aux/config.guess)
    make
    make DESTDIR=$LFS install
    finish_build findutils-4.8.0.tar.xz
    touch findutils-4.8.0.tar.xz.built
fi

if [ ! -f gawk-5.1.0.tar.xz.built ]; then
    prepare_build gawk-5.1.0.tar.xz
    sed -i 's/extras//' Makefile.in
    ./configure --prefix=/usr   \
    --host=$LFS_TGT \
    --build=$(./config.guess)
    make
    make DESTDIR=$LFS install
    finish_build gawk-5.1.0.tar.xz
    touch gawk-5.1.0.tar.xz.built
fi

if [ ! -f grep-3.7.tar.xz.built ]; then
    prepare_build grep-3.7.tar.xz
    ./configure --prefix=/usr   \
    --host=$LFS_TGT
    make
    make DESTDIR=$LFS install
    finish_build grep-3.7.tar.xz
    touch grep-3.7.tar.xz.built
fi

if [ ! -f gzip-1.10.tar.xz.built ]; then
    prepare_build gzip-1.10.tar.xz
    ./configure --prefix=/usr   \
    --host=$LFS_TGT
    make
    make DESTDIR=$LFS install
    finish_build gzip-1.10.tar.xz
    touch gzip-1.10.tar.xz.built
fi

if [ ! -f make-4.3.tar.gz.built ]; then
    prepare_build make-4.3.tar.gz
    ./configure --prefix=/usr   \
    --without-guile \
    --host=$LFS_TGT \
    --build=$(build-aux/config.guess)
    make
    make DESTDIR=$LFS install
    finish_build make-4.3.tar.gz
    touch make-4.3.tar.gz.built
fi

if [ ! -f patch-2.7.6.tar.xz.built ]; then
    prepare_build patch-2.7.6.tar.xz
    ./configure --prefix=/usr   \
    --host=$LFS_TGT \
    --build=$(build-aux/config.guess)
    make
    make DESTDIR=$LFS install
    finish_build patch-2.7.6.tar.xz
    touch patch-2.7.6.tar.xz.built
fi

if [ ! -f sed-4.8.tar.xz.built ]; then
    prepare_build sed-4.8.tar.xz
    ./configure --prefix=/usr   \
    --host=$LFS_TGT
    make
    make DESTDIR=$LFS install
    finish_build sed-4.8.tar.xz
    touch sed-4.8.tar.xz.built
fi

if [ ! -f tar-1.34.tar.xz.built ]; then
    prepare_build tar-1.34.tar.xz
    ./configure --prefix=/usr \
    --host=$LFS_TGT \
    --build=$(build-aux/config.guess)
    make
    make DESTDIR=$LFS install
    finish_build tar-1.34.tar.xz
    touch tar-1.34.tar.xz.built
fi

if [ ! -f xz-5.2.5.tar.xz.built ]; then
    prepare_build xz-5.2.5.tar.xz
    ./configure --prefix=/usr   \
    --host=$LFS_TGT \
    --build=$(build-aux/config.guess) \
    --disable-static \
    --docdir=/usr/share/doc/xz-5.2.5
    make
    make DESTDIR=$LFS install
    finish_build xz-5.2.5.tar.xz
    touch xz-5.2.5.tar.xz.built
fi

if [ ! -f binutils-2.37.tar.xz.built-pass2 ]; then
    prepare_build binutils-2.37.tar.xz
    mkdir -v build
    cd       build
    ../configure                   \
    --prefix=/usr              \
    --build=$(../config.guess) \
    --host=$LFS_TGT            \
    --disable-nls              \
    --enable-shared            \
    --disable-werror           \
    --enable-64-bit-bfd
    make
    make DESTDIR=$LFS install -j1
    install -vm755 libctf/.libs/libctf.so.0.0.0 $LFS/usr/lib
    finish_build binutils-2.37.tar.xz
    touch binutils-2.37.tar.xz.built-pass2
fi

if [ ! -f gcc-11.2.0.tar.xz.built-pass2 ]; then
    prepare_build gcc-11.2.0.tar.xz
    tar -xf ../mpfr-4.1.0.tar.xz
    mv -v mpfr-4.1.0 mpfr
    tar -xf ../gmp-6.2.1.tar.xz
    mv -v gmp-6.2.1 gmp
    tar -xf ../mpc-1.2.1.tar.gz
    mv -v mpc-1.2.1 mpc
    case $(uname -m) in
        x86_64)
            sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
        ;;
    esac
    mkdir -v build
    cd       build
    mkdir -pv $LFS_TGT/libgcc
    ln -s ../../../libgcc/gthr-posix.h $LFS_TGT/libgcc/gthr-default.h
    ../configure                                       \
    --build=$(../config.guess)                     \
    --host=$LFS_TGT                                \
    --prefix=/usr                                  \
    CC_FOR_TARGET=$LFS_TGT-gcc                     \
    --with-build-sysroot=$LFS                      \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++
    make
    make DESTDIR=$LFS install
    ln -sv gcc $LFS/usr/bin/cc
    finish_build gcc-11.2.0.tar.xz
    touch gcc-11.2.0.tar.xz.built-pass2
fi

popd

echo "Now login as root and run lfs4.sh."