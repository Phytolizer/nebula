#!/usr/bin/env bash
set -xeuo pipefail

prepare_build() {
    tar xf $1
    pushd ${1%.tar.*}
}

finish_build() {
    popd
    rm -rf ${1%.tar.*}
}

pushd /sources

if [ ! -f libtool-2.4.6.tar.xz.built ]; then
    prepare_build libtool-2.4.6.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build libtool-2.4.6.tar.xz
    touch libtool-2.4.6.tar.xz.built
fi

if [ ! -f gdbm-1.20.tar.gz.built ]; then
    prepare_build gdbm-1.20.tar.gz
    ./configure --prefix=/usr --disable-static --enable-libgdbm-compat
    make
    make install
    finish_build gdbm-1.20.tar.gz
    touch gdbm-1.20.tar.gz.built
fi

if [ ! -f gperf-3.1.tar.gz.built ]; then
    prepare_build gperf-3.1.tar.gz
    ./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1
    make
    make install
    finish_build gperf-3.1.tar.gz
    touch gperf-3.1.tar.gz.built
fi

if [ ! -f expat-2.4.1.tar.xz.built ]; then
    prepare_build expat-2.4.1.tar.xz
    ./configure --prefix=/usr --disable-static --docdir=/usr/share/doc/expat-2.4.1
    make
    make install
    install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.4.1
    finish_build expat-2.4.1.tar.xz
    touch expat-2.4.1.tar.xz.built
fi

if [ ! -f inetutils-2.1.tar.xz.built ]; then
    prepare_build inetutils-2.1.tar.xz
    ./configure --prefix=/usr        \
    --bindir=/usr/bin    \
    --localstatedir=/var \
    --disable-logger     \
    --disable-whois      \
    --disable-rcp        \
    --disable-rexec      \
    --disable-rlogin     \
    --disable-rsh        \
    --disable-servers
    make
    make install
    mv -v /usr/{,s}bin/ifconfig
    finish_build inetutils-2.1.tar.xz
    touch inetutils-2.1.tar.xz.built
fi

if [ ! -f less-590.tar.gz.built ]; then
    prepare_build less-590.tar.gz
    ./configure --prefix=/usr --sysconfdir=/etc
    make
    make install
    finish_build less-590.tar.gz
    touch less-590.tar.gz.built
fi

if [ ! -f perl-5.34.0.tar.xz.built2 ]; then
    prepare_build perl-5.34.0.tar.xz
    patch -Np1 -i ../perl-5.34.0-upstream_fixes-1.patch
    export BUILD_ZLIB=False
    export BUILD_BZIP2=0
    sh Configure -des                                         \
    -Dprefix=/usr                                \
    -Dvendorprefix=/usr                          \
    -Dprivlib=/usr/lib/perl5/5.34/core_perl      \
    -Darchlib=/usr/lib/perl5/5.34/core_perl      \
    -Dsitelib=/usr/lib/perl5/5.34/site_perl      \
    -Dsitearch=/usr/lib/perl5/5.34/site_perl     \
    -Dvendorlib=/usr/lib/perl5/5.34/vendor_perl  \
    -Dvendorarch=/usr/lib/perl5/5.34/vendor_perl \
    -Dman1dir=/usr/share/man/man1                \
    -Dman3dir=/usr/share/man/man3                \
    -Dpager="/usr/bin/less -isR"                 \
    -Duseshrplib                                 \
    -Dusethreads
    make
    make install
    unset BUILD_ZLIB BUILD_BZIP2
    finish_build perl-5.34.0.tar.xz
    touch perl-5.34.0.tar.xz.built2
fi

if [ ! -f XML-Parser-2.46.tar.gz.built ]; then
    prepare_build XML-Parser-2.46.tar.gz
    perl Makefile.PL
    make
    make install
    finish_build XML-Parser-2.46.tar.gz
    touch XML-Parser-2.46.tar.gz.built
fi

if [ ! -f intltool-0.51.0.tar.gz.built ]; then
    prepare_build intltool-0.51.0.tar.gz
    sed -i 's:\\\${:\\\$\\{:' intltool-update.in
    ./configure --prefix=/usr
    make
    make install
    install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO
    finish_build intltool-0.51.0.tar.gz
    touch intltool-0.51.0.tar.gz.built
fi

if [ ! -f autoconf-2.71.tar.xz.built ]; then
    prepare_build autoconf-2.71.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build autoconf-2.71.tar.xz
    touch autoconf-2.71.tar.xz.built
fi

if [ ! -f automake-1.16.4.tar.xz.built ]; then
    prepare_build automake-1.16.4.tar.xz
    ./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.4
    make
    make install
    finish_build automake-1.16.4.tar.xz
    touch automake-1.16.4.tar.xz.built
fi

if [ ! -f kmod-29.tar.xz.built ]; then
    prepare_build kmod-29.tar.xz
    ./configure --prefix=/usr          \
    --sysconfdir=/etc      \
    --with-xz              \
    --with-zstd            \
    --with-zlib
    make
    make install
    
    for target in depmod insmod modinfo modprobe rmmod; do
        ln -sfv ../bin/kmod /usr/sbin/$target
    done
    
    ln -sfv kmod /usr/bin/lsmod
    finish_build kmod-29.tar.xz
    touch kmod-29.tar.xz.built
fi

if [ ! -f elfutils-0.185.tar.bz2.built ]; then
    prepare_build elfutils-0.185.tar.bz2
    ./configure --prefix=/usr \
    --disable-debuginfod \
    --enable-libdebuginfod=dummy
    make
    make -C libelf install
    install -vm644 config/libelf.pc /usr/lib/pkgconfig
    rm /usr/lib/libelf.a
    finish_build elfutils-0.185.tar.bz2
    touch elfutils-0.185.tar.bz2.built
fi

if [ ! -f libffi-3.4.2.tar.gz.built ]; then
    prepare_build libffi-3.4.2.tar.gz
    ./configure --prefix=/usr --disable-static --with-gcc-arch=native --disable-exec-static-tramp
    make
    make install
    finish_build libffi-3.4.2.tar.gz
    touch libffi-3.4.2.tar.gz.built
fi

if [ ! -f openssl-1.1.1l.tar.gz.built ]; then
    prepare_build openssl-1.1.1l.tar.gz
    ./config --prefix=/usr         \
    --openssldir=/etc/ssl \
    --libdir=lib          \
    shared                \
    zlib-dynamic
    make
    sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
    make MANSUFFIX=ssl install
    mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1l
    cp -vfr doc/* /usr/share/doc/openssl-1.1.1l
    finish_build openssl-1.1.1l.tar.gz
    touch openssl-1.1.1l.tar.gz.built
fi

if [ ! -f Python-3.9.6.tar.xz.built2 ]; then
    prepare_build Python-3.9.6.tar.xz
    ./configure --prefix=/usr        \
    --enable-shared      \
    --with-system-expat  \
    --with-system-ffi    \
    --with-ensurepip=yes \
    --enable-optimizations
    make
    make install
    install -v -dm755 /usr/share/doc/python-3.9.6/html
    
    tar --strip-components=1  \
    --no-same-owner       \
    --no-same-permissions \
    -C /usr/share/doc/python-3.9.6/html \
    -xvf ../python-3.9.6-docs-html.tar.bz2
    finish_build Python-3.9.6.tar.xz
    touch Python-3.9.6.tar.xz.built2
fi

if [ ! -f ninja-1.10.2.tar.gz.built ]; then
    prepare_build ninja-1.10.2.tar.gz
    python3 configure.py --bootstrap
    install -vm755 ninja /usr/bin/
    install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
    install -vDm644 misc/zsh-completion  /usr/share/zsh/site-functions/_ninja
    finish_build ninja-1.10.2.tar.gz
    touch ninja-1.10.2.tar.gz.built
fi

if [ ! -f meson-0.59.1.tar.gz.built ]; then
    prepare_build meson-0.59.1.tar.gz
    python3 setup.py build
    python3 setup.py install --root=dest
    cp -rv dest/* /
    install -vDm644 data/shell-completions/bash/meson /usr/share/bash-completion/completions/meson
    install -vDm644 data/shell-completions/zsh/_meson /usr/share/zsh/site-functions/_meson
    finish_build meson-0.59.1.tar.gz
    touch meson-0.59.1.tar.gz.built
fi

if [ ! -f coreutils-8.32.tar.xz.built2 ]; then
    prepare_build coreutils-8.32.tar.xz
    patch -Np1 -i ../coreutils-8.32-i18n-1.patch
    autoreconf -fiv
    FORCE_UNSAFE_CONFIGURE=1 ./configure \
    --prefix=/usr            \
    --enable-no-install-program=kill,uptime
    make
    make install
    mv -v /usr/bin/chroot /usr/sbin
    mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
    sed -i 's/"1"/"8"/' /usr/share/man/man8/chroot.8
    finish_build coreutils-8.32.tar.xz
    touch coreutils-8.32.tar.xz.built2
fi

if [ ! -f check-0.15.2.tar.gz.built ]; then
    prepare_build check-0.15.2.tar.gz
    ./configure --prefix=/usr --disable-static
    make
    make docdir=/usr/share/doc/check-0.15.2 install
    finish_build check-0.15.2.tar.gz
    touch check-0.15.2.tar.gz.built
fi

if [ ! -f diffutils-3.8.tar.xz.built2 ]; then
    prepare_build diffutils-3.8.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build diffutils-3.8.tar.xz
    touch diffutils-3.8.tar.xz.built2
fi

if [ ! -f gawk-5.1.0.tar.xz.built2 ]; then
    prepare_build gawk-5.1.0.tar.xz
    sed -i 's/extras//' Makefile.in
    ./configure --prefix=/usr
    make
    make install
    mkdir -v /usr/share/doc/gawk-5.1.0
    cp    -v doc/{awkforai.txt,*.{eps,pdf,jpg}} /usr/share/doc/gawk-5.1.0
    finish_build gawk-5.1.0.tar.xz
    touch gawk-5.1.0.tar.xz.built2
fi

if [ ! -f findutils-4.8.0.tar.xz.built2 ]; then
    prepare_build findutils-4.8.0.tar.xz
    ./configure --prefix=/usr --localstatedir=/var/lib/locate
    make
    make install
    finish_build findutils-4.8.0.tar.xz
    touch findutils-4.8.0.tar.xz.built2
fi

if [ ! -f groff-1.22.4.tar.gz.built ]; then
    prepare_build groff-1.22.4.tar.gz
    PAGE=letter ./configure --prefix=/usr
    make -j1
    make install
    finish_build groff-1.22.4.tar.gz
    touch groff-1.22.4.tar.gz.built
fi

if [ ! -f grub-2.06.tar.xz.built ]; then
    prepare_build grub-2.06.tar.xz
    ./configure --prefix=/usr          \
    --sysconfdir=/etc      \
    --disable-efiemu       \
    --disable-werror
    make
    make install
    mv -v /etc/bash_completion.d/grub /usr/share/bash-completion/completions
    finish_build grub-2.06.tar.xz
    touch grub-2.06.tar.xz.built
fi

if [ ! -f gzip-1.10.tar.xz.built2 ]; then
    prepare_build gzip-1.10.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build gzip-1.10.tar.xz
    touch gzip-1.10.tar.xz.built2
fi

if [ ! -f iproute2-5.13.0.tar.xz.built ]; then
    prepare_build iproute2-5.13.0.tar.xz
    sed -i /ARPD/d Makefile
    rm -fv man/man8/arpd.8
    sed -i 's/.m_ipt.o//' tc/Makefile
    make
    make SBINDIR=/usr/sbin install
    mkdir -pv              /usr/share/doc/iproute2-5.13.0
    cp -v COPYING README* /usr/share/doc/iproute2-5.13.0
    finish_build iproute2-5.13.0.tar.xz
    touch iproute2-5.13.0.tar.xz.built
fi

if [ ! -f kbd-2.4.0.tar.xz.built ]; then
    prepare_build kbd-2.4.0.tar.xz
    patch -Np1 -i ../kbd-2.4.0-backspace-1.patch
    sed -i '/RESIZECONS_PROGS=/s/yes/no/' configure
    sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in
    ./configure --prefix=/usr --disable-vlock
    make
    make install
    mkdir -v            /usr/share/doc/kbd-2.4.0
    cp -R -v docs/doc/* /usr/share/doc/kbd-2.4.0
    finish_build kbd-2.4.0.tar.xz
    touch kbd-2.4.0.tar.xz.built
fi

if [ ! -f libpipeline-1.5.3.tar.gz.built ]; then
    prepare_build libpipeline-1.5.3.tar.gz
    ./configure --prefix=/usr
    make
    make install
    finish_build libpipeline-1.5.3.tar.gz
    touch libpipeline-1.5.3.tar.gz.built
fi

if [ ! -f make-4.3.tar.gz.built2 ]; then
    prepare_build make-4.3.tar.gz
    ./configure --prefix=/usr
    make
    make install
    finish_build make-4.3.tar.gz
    touch make-4.3.tar.gz.built2
fi

if [ ! -f patch-2.7.6.tar.xz.built2 ]; then
    prepare_build patch-2.7.6.tar.xz
    ./configure --prefix=/usr
    make
    make install
    finish_build patch-2.7.6.tar.xz
    touch patch-2.7.6.tar.xz.built2
fi

if [ ! -f tar-1.34.tar.xz.built2 ]; then
    prepare_build tar-1.34.tar.xz
    FORCE_UNSAFE_CONFIGURE=1 ./configure --prefix=/usr
    make
    make install
    make -C doc install-html docdir=/usr/share/doc/tar-1.34
    finish_build tar-1.34.tar.xz
    touch tar-1.34.tar.xz.built2
fi

if [ ! -f texinfo-6.8.tar.xz.built2 ]; then
    prepare_build texinfo-6.8.tar.xz
    ./configure --prefix=/usr --disable-static
    sed -e 's/__attribute_nonnull__/__nonnull/' \
    -i gnulib/lib/malloc/dynarray-skeleton.c
    make
    make install
    make TEXMF=/usr/share/texmf install-tex
    finish_build texinfo-6.8.tar.xz
    touch texinfo-6.8.tar.xz.built2
fi

if [ ! -f vim-8.2.3337.tar.gz.built ]; then
    prepare_build vim-8.2.3337.tar.gz
    echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
    ./configure --prefix=/usr
    make
    make install
    ln -sv vim /usr/bin/vi
    for L in  /usr/share/man/{,*/}man1/vim.1; do
        ln -sv vim.1 $(dirname $L)/vi.1
    done
    ln -sv ../vim/vim82/doc /usr/share/doc/vim-8.2.3337
    finish_build vim-8.2.3337.tar.gz
    touch vim-8.2.3337.tar.gz.built
fi

cat > /etc/vimrc << "EOF"
" Begin /etc/vimrc

" Ensure defaults are set before customizing settings, not after
source $VIMRUNTIME/defaults.vim
let skip_defaults_vim=1

set nocompatible
set backspace=2
set mouse=
syntax on
if (&term == "xterm") || (&term == "putty")
  set background=dark
endif

" End /etc/vimrc
EOF

if [ ! -f eudev-3.2.10.tar.gz.built ]; then
    prepare_build eudev-3.2.10.tar.gz
    ./configure --prefix=/usr           \
    --bindir=/usr/sbin      \
    --sysconfdir=/etc       \
    --enable-manpages       \
    --disable-static
    make
    mkdir -pv /usr/lib/udev/rules.d
    mkdir -pv /etc/udev/rules.d
    make install
    tar -xvf ../udev-lfs-20171102.tar.xz
    make -f udev-lfs-20171102/Makefile.lfs install
    udevadm hwdb --update
    finish_build eudev-3.2.10.tar.gz
    touch eudev-3.2.10.tar.gz.built
fi

if [ ! -f man-db-2.9.4.tar.xz.built ]; then
    prepare_build man-db-2.9.4.tar.xz
    ./configure --prefix=/usr                        \
    --docdir=/usr/share/doc/man-db-2.9.4 \
    --sysconfdir=/etc                    \
    --disable-setuid                     \
    --enable-cache-owner=bin             \
    --with-browser=/usr/bin/lynx         \
    --with-vgrind=/usr/bin/vgrind        \
    --with-grap=/usr/bin/grap            \
    --with-systemdtmpfilesdir=           \
    --with-systemdsystemunitdir=
    make
    make install
    finish_build man-db-2.9.4.tar.xz
    touch man-db-2.9.4.tar.xz.built
fi

if [ ! -f procps-ng-3.3.17.tar.xz.built ]; then
    tar xf procps-ng-3.3.17.tar.xz
    pushd procps-3.3.17
    ./configure --prefix=/usr                            \
    --docdir=/usr/share/doc/procps-ng-3.3.17 \
    --disable-static                         \
    --disable-kill
    make
    make install
    rm -rf procps-3.3.17
    popd
    touch procps-ng-3.3.17.tar.xz.built
fi

if [ ! -f util-linux-2.37.2.tar.xz.built2 ]; then
    prepare_build util-linux-2.37.2.tar.xz
    ./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --libdir=/usr/lib    \
            --docdir=/usr/share/doc/util-linux-2.37.2 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --without-systemd    \
            --without-systemdsystemunitdir \
            runstatedir=/run
    make
    make install
    finish_build util-linux-2.37.2.tar.xz
    touch util-linux-2.37.2.tar.xz.built2
fi

if [ ! -f e2fsprogs-1.46.4.tar.gz.built ]; then
    prepare_build e2fsprogs-1.46.4.tar.gz
    mkdir -v build
    cd build
    ../configure --prefix=/usr           \
             --sysconfdir=/etc       \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck
    make
    make install
    rm -fv /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
    gunzip -v /usr/share/info/libext2fs.info.gz
    install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info
    makeinfo -o      doc/com_err.info ../lib/et/com_err.texinfo
    install -v -m644 doc/com_err.info /usr/share/info
    install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info
    finish_build e2fsprogs-1.46.4.tar.gz
    touch e2fsprogs-1.46.4.tar.gz.built
fi

if [ ! -f sysklogd-1.5.1.tar.gz.built ]; then
    prepare_build sysklogd-1.5.1.tar.gz
    sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
    sed -i 's/union wait/int/' syslogd.c
    make
    make BINDIR=/sbin install
    finish_build sysklogd-1.5.1.tar.gz
    touch sysklogd-1.5.1.tar.gz.built
fi

cat > /etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf

auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *

# End /etc/syslog.conf
EOF

if [ ! -f sysvinit-2.99.tar.xz.built ]; then
    prepare_build sysvinit-2.99.tar.xz
    patch -Np1 -i ../sysvinit-2.99-consolidated-1.patch
    make
    make install
    finish_build sysvinit-2.99.tar.xz
    touch sysvinit-2.99.tar.xz.built
fi

echo "==== CLEANING UP ===="
rm -rf /tmp/*

echo "Relog by copying the below command, then run lfs9.sh:"
echo "logout"
cat << "EOF"
chroot "$LFS" /usr/bin/env -i          \
    HOME=/root TERM="$TERM"            \
    PS1='(lfs chroot) \u:\w\$ '        \
    PATH=/usr/bin:/usr/sbin            \
    /bin/bash --login
EOF

popd