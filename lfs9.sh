#!/usr/bin/env bash
set -xeuo pipefail

find /usr/lib /usr/libexec -name \*.la -delete
find /usr -depth -name $(uname -m)-lfs-linux-gnu\* | xargs rm -rf
userdel -r tester || true

prepare_build() {
    tar xf $1
    pushd ${1%.tar.*}
}

finish_build() {
    popd
    rm -rf ${1%.tar.*}
}

pushd /sources

if [ ! -f lfs-bootscripts-20210608.tar.xz.built ]; then
    prepare_build lfs-bootscripts-20210608.tar.xz
    make install
    finish_build lfs-bootscripts-20210608.tar.xz
    touch lfs-bootscripts-20210608.tar.xz.built
fi

popd

bash /usr/lib/udev/init-net-rules.sh || true

cat /etc/udev/rules.d/70-persistent-net.rules
read -p "Press [Enter] key to continue..."

cd /etc/sysconfig/
cat > ifconfig.eth0 << "EOF"
ONBOOT=yes
IFACE=enp0s31f6
SERVICE=ipv4-static
IP=192.168.1.2
GATEWAY=192.168.1.1
PREFIX=24
BROADCAST=192.168.1.255
EOF

cat > /etc/resolv.conf << "EOF"
# Begin /etc/resolv.conf

domain local
nameserver 8.8.8.8
nameserver 8.8.4.4

# End /etc/resolv.conf
EOF

echo "nebula" > /etc/hostname

cat > /etc/hosts << "EOF"
# Begin /etc/hosts

127.0.0.1 localhost.localdomain localhost
::1       localhost ip6-localhost ip6-loopback
ff02::1   ip6-allnodes
ff02::2   ip6-allrouters

# End /etc/hosts
EOF

cat > /etc/inittab << "EOF"
# Begin /etc/inittab

id:3:initdefault:

si::sysinit:/etc/rc.d/init.d/rc S

l0:0:wait:/etc/rc.d/init.d/rc 0
l1:S1:wait:/etc/rc.d/init.d/rc 1
l2:2:wait:/etc/rc.d/init.d/rc 2
l3:3:wait:/etc/rc.d/init.d/rc 3
l4:4:wait:/etc/rc.d/init.d/rc 4
l5:5:wait:/etc/rc.d/init.d/rc 5
l6:6:wait:/etc/rc.d/init.d/rc 6

ca:12345:ctrlaltdel:/sbin/shutdown -t1 -a -r now

su:S016:once:/sbin/sulogin

1:2345:respawn:/sbin/agetty --noclear tty1 9600
2:2345:respawn:/sbin/agetty tty2 9600
3:2345:respawn:/sbin/agetty tty3 9600
4:2345:respawn:/sbin/agetty tty4 9600
5:2345:respawn:/sbin/agetty tty5 9600
6:2345:respawn:/sbin/agetty tty6 9600

# End /etc/inittab
EOF

cat > /etc/sysconfig/clock << "EOF"
# Begin /etc/sysconfig/clock

UTC=1

# Set this to any options you might need to give to hwclock,
# such as machine hardware clock type for Alphas.
CLOCKPARAMS=

# End /etc/sysconfig/clock
EOF

locale -a

cat > /etc/profile << "EOF"
# Begin /etc/profile

export LANG=en_US.UTF-8

# End /etc/profile
EOF

cat > /etc/inputrc << "EOF"
# Begin /etc/inputrc
# Modified by Chris Lynn <roryo@roryo.dynup.net>

# Allow the command prompt to wrap to the next line
set horizontal-scroll-mode Off

# Enable 8bit input
set meta-flag On
set input-meta On

# Turns off 8th bit stripping
set convert-meta Off

# Keep the 8th bit for display
set output-meta On

# none, visible or audible
set bell-style none

# All of the following map the escape sequence of the value
# contained in the 1st argument to the readline specific functions
"\eOd": backward-word
"\eOc": forward-word

# for linux console
"\e[1~": beginning-of-line
"\e[4~": end-of-line
"\e[5~": beginning-of-history
"\e[6~": end-of-history
"\e[3~": delete-char
"\e[2~": quoted-insert

# for xterm
"\eOH": beginning-of-line
"\eOF": end-of-line

# for Konsole
"\e[H": beginning-of-line
"\e[F": end-of-line

# End /etc/inputrc
EOF

cat > /etc/shells << "EOF"
# Begin /etc/shells

/bin/sh
/bin/bash

# End /etc/shells
EOF

cat > /etc/fstab << "EOF"
# Begin /etc/fstab

# file system  mount-point  type     options             dump  fsck
#                                                              order

/dev/sda2      /            ext4     defaults            1     1
/dev/sda1      /boot        fat32    defaults            0     0
proc           /proc        proc     nosuid,noexec,nodev 0     0
sysfs          /sys         sysfs    nosuid,noexec,nodev 0     0
devpts         /dev/pts     devpts   gid=5,mode=620      0     0
tmpfs          /run         tmpfs    defaults            0     0
devtmpfs       /dev         devtmpfs mode=0755,nosuid    0     0

# End /etc/fstab
EOF

pushd /sources

if [ ! -f linux-5.13.12.tar.xz.built ]; then
    prepare_build linux-5.13.12.tar.xz
    make mrproper
    make defconfig
    make menuconfig
    make
    make modules_install
    echo "In a separate window on the host system, run the following command."
    echo "mount --bind /boot /mnt/lfs/boot"
    read -p "Press [Enter] key to continue..."
    cp -iv arch/x86/boot/bzImage /boot/vmlinuz-5.13.12-lfs-11.0
    cp -iv System.map /boot/System.map-5.13.12
    cp -iv .config /boot/config-5.13.12
    install -d /usr/share/doc/linux-5.13.12
    cp -r Documentation/* /usr/share/doc/linux-5.13.12
    finish_build linux-5.13.12.tar.xz
    touch linux-5.13.12.tar.xz.built
fi

popd

install -v -m755 -d /etc/modprobe.d
cat > /etc/modprobe.d/usb.conf << "EOF"
# Begin /etc/modprobe.d/usb.conf

install ohci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i ohci_hcd ; true
install uhci_hcd /sbin/modprobe ehci_hcd ; /sbin/modprobe -i uhci_hcd ; true

# End /etc/modprobe.d/usb.conf
EOF

echo "Go ahead and modify GRUB to add the LFS entry."
read -p "Press [Enter] key to continue..."

echo 11.0 > /etc/lfs-release

cat > /etc/lsb-release << "EOF"
DISTRIB_ID="Nebula Linux"
DISTRIB_RELEASE="1.0"
DISTRIB_CODENAME="Nebula"
DISTRIB_DESCRIPTION="Linux From Scratch"
EOF

cat > /etc/os-release << "EOF"
NAME="Nebula Linux"
VERSION="1.0"
ID=nebula
PRETTY_NAME="Nebula Linux 1.0"
VERSION_CODENAME="Nebula"
EOF

echo "Congratulations! Nebula Linux was installed."
echo "Now I will install some optional but helpful packages."
read -r -p "Continue? [Y/n] " response
if [[ $response =~ ^([nN][oO]|[nN])$ ]]; then
    exit 0
fi

pushd /sources

if [ ! -f lynx2.8.9rel.1.tar.bz2 ]; then
    wget https://invisible-mirror.net/archives/lynx/tarballs/lynx2.8.9rel.1.tar.bz2
fi

if [ ! -f lynx2.8.9.rel.1.tar.bz2.built ]; then
    prepare_build lynx2.8.9rel.1.tar.bz2
    ./configure --prefix=/usr          \
    --sysconfdir=/etc/lynx \
    --datadir=/usr/share/doc/lynx-2.8.9rel.1 \
    --with-zlib            \
    --with-bzlib           \
    --with-ssl             \
    --with-screen=ncursesw \
    --enable-locale-charset &&
    make
    make install-full
    chgrp -v -R root /usr/share/doc/lynx-2.8.9rel.1/lynx_doc
    finish_build lynx2.8.9rel.1.tar.bz2
    touch lynx2.8.9rel.1.tar.bz2.built
fi
