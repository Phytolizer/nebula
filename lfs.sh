#!/bin/bash
set -xeuo pipefail

# 2.6
export LFS=/mnt/lfs
echo $LFS

# 2.7
mkdir -pv $LFS
if mountpoint -q $LFS; then
    echo "LFS is mounted"
else
    echo "LFS is not mounted"
    mount -v -t ext4 /dev/sda2 $LFS
fi

mkdir -pv $LFS/boot
if mountpoint -q $LFS/boot; then
    echo "LFS/boot is mounted"
else
    echo "LFS/boot is not mounted"
    mount -v /dev/sda1 $LFS/boot
fi

## CHAPTER 3

# 3.1
mkdir -pv $LFS/sources
chmod -v a+wt $LFS/sources

wget --input-file=/home/phyto/Downloads/wget-list --continue --directory-prefix=$LFS/sources
pushd $LFS/sources
md5sum -c /home/phyto/Downloads/md5sums
popd

## CHAPTER 4
mkdir -pv $LFS/{etc,var} $LFS/usr/{bin,lib,sbin}

for i in bin lib sbin; do
    ln -sv usr/$i $LFS/$i
done

case $(uname -m) in
    x86_64) mkdir -pv $LFS/lib64 ;;
esac

# 4.2
mkdir -pv $LFS/{etc,var} $LFS/usr/{bin,lib,sbin}

for i in bin lib sbin; do
    ln -sv usr/$i $LFS/$i
done

case $(uname -m) in
    x86_64) mkdir -pv $LFS/lib64 ;;
esac

mkdir -pv $LFS/tools

if id lfs &>/dev/null; then
    echo "lfs user already exists"
else
    groupadd lfs
    useradd -s /bin/bash -g lfs -m -k /dev/null lfs
fi

chown -v lfs $LFS/{usr{,/*},lib,var,etc,bin,sbin,tools}
case $(uname -m) in
    x86_64) chown -v lfs $LFS/lib64 ;;
esac

chown -v lfs $LFS/sources

echo "Now run lfs2.sh."
su - lfs
